# Radboud Palette

Trying to recreate these rather sad images with the Radboud color palette as listed here:
<https://www.ru.nl/medewerkers/services/diensten-en-faciliteiten/communicatie-en-promotie/vormgeving-en-opmaak/huisstijl/kleur>

![Red Impact](img/red-impact-huisstijl-radboud-universiteit.png "Red Impact")

![Poppy, Lady bug, Berry, Maroon, Mahogany](img/primaire-kleuren-huisstijl-radboud-universiteit.png "Primaire kleuren Radboud Universiteit")


I'm not sure about the font colors. Let me know if you have any suggestions ;-)

Red Impact ..act ..act:

![Red Impact](img/RED-IMPACT.png "RED IMPACT")

## XKCD

There's an XKCD for every situation.

![Digital Data](https://imgs.xkcd.com/comics/digital_data.png)

## Install and run

Tested with Ubuntu 22.04. Install imagemagick and the open sans font:

``` console
sudo apt install imagemagick fonts-open-sans
```

Run

``` console
./gen_palette img
```

## Result


### Red Impact

![Red Impact](img/Red-Impact.png "Red Impact")

Hex colors:
- font color: `#000000`
- background color: `#e3000b`

### Poppy

![Poppy](img/Poppy.png "Poppy")

Hex colors:
- font color: `#730e04`
- background color: `#ff424b`

### Lady bug

![Lady bug](img/Lady-bug.png "Lady bug")

Hex colors:
- font color: `#ff424b`
- background color: `#be311e`

### Berry

![Berry](img/Berry.png "Berry")

Hex colors:
- font color: `#e3000b`
- background color: `#8f2011`

### Maroon

![Maroon](img/Maroon.png "Maroon")

Hex colors:
- font color: `#ff424b`
- background color: `#730e04`

### Mahogany

![Mahogany](img/Mahogany.png "Mahogany")

Hex colors:
- font color: `#e3000b`
- background color: `#4a0004`


## Open Sans font

A similar image is hosted here <https://www.ru.nl/medewerkers/services/diensten-en-faciliteiten/communicatie-en-promotie/vormgeving-en-opmaak/huisstijl/lettertype>.
![Open Sans](img/opensans.png "Open Sans")
